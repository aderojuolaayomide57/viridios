import React, { Component, useState,useEffect } from "react";

import Link from 'next/link';
import Head from 'next/head';
import Image from 'next/image';

import Layout from '../components/layout';

import styles from '../styles/Home.module.css';
import { default as ReactSelect } from "react-select";
import { components } from "react-select";
import makeAnimated from "react-select/animated";
import MySelect from "../components/MySelect";


import SDG from '../public/ressources/viri';
import {countries} from '../public/ressources/sdgcountries';


const Option = props => {
  return (
    <div>
      <components.Option {...props}>
        <input
          type="checkbox"
          checked={props.isSelected}
          onChange={() => null}
        />{" "}
        <label>{props.label}</label>
      </components.Option>
    </div>
  );
};

const MultiValue = props => (
  <components.MultiValue {...props}>
    <span>{props.data.label}</span>
  </components.MultiValue>
);

const animatedComponents = makeAnimated();


const renderTableData = (optionSelected) => {
  return optionSelected && optionSelected.map((sdg1,index) => {
     return (
      <tr key={index}>
          <td>{sdg1.value}</td>
          <td>{sdg1.label}</td>
          <td>{sdg1.score}</td>
          <td>{sdg1.rank}</td>
          <td>{sdg1.region}</td>
          <td>{sdg1.spillover}</td>
          <td>{sdg1.goal1dash}</td>
      </tr>
     )
  })
}


const Home = () => {

  const [optionSelected, handleChange] = useState(null);
  const [ave, handleAverage] = useState(0);


  useEffect(() => {
    console.log(optionSelected);
    
  },[optionSelected, ave]);

  
  return (
    <Layout
      home={true}
    >
      <div className="container">
      <MySelect
        options={countries}
        isMulti
        closeMenuOnSelect={false}
        hideSelectedOptions={false}
        components={{ Option, MultiValue, animatedComponents }}
        onChange={(selected) => handleChange(selected)}
        allowSelectAll={true}
        value={optionSelected}
      />
      </div>

<br/><br/><br/>
      <div className={styles.sdg}>
      <table>
        <thead>
            <tr>
                <td>Country name</td>
                <td>Country code</td>
                <td>SDG index score (2020)</td>
                <td>SDG index rank (2020)</td>
                <td>Region</td>
                <td>Regional Score</td>
                <td>Color code for SDG 1 - 17</td>
            </tr>
        </thead>
        <tbody>
          {renderTableData(optionSelected, handleAverage, ave)}
          <tr>
                <td></td>
                <td></td>
                <td>{optionSelected && console.log(optionSelected.reduce((partial_sum, value) => {partial_sum + value.score}, 0))}</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </tbody>
    </table>

      </div>
    </Layout>
  )  
}

export default Home
