import React, { Component, useState,useEffect } from "react";

import Link from 'next/link';
import Head from 'next/head';
import Image from 'next/image';

import Layout from '../components/layout';

import Chart from 'chart.js/auto';
import { Bar, CategoryScale} from "react-chartjs-2";

//Chart.register(CategoryScale)

const Chart1 = ({chartData}) => {
  return (
    <Bar
        data={chartData}
        options={{
          plugins: {
            title: {
              display: true,
              text: "Carbon Intensity"
            },
            legend: {
              display: true,
              position: "bottom"
           }
          }
        }}
      />
  )
}


const Intensity = () => {

    const [chartData, setChartData] = useState(null)
    const [isLoading, loadingData] = useState(false)

    const date = '2021-01-20'

    useEffect(() => {

        const CarbonIntensity = async () => {
          loadingData({isLoading: false})
          const res = await fetch(`https://api.carbonintensity.org.uk/intensity/date/${date}`);
          const data = await res.json()
          if(data){
              loadingData({isLoading: true})
              const labels = [];
              const datas = [];
              data.data.map((intense) => {
                labels.push(intense.from)
                datas.push(intense.intensity.forecast)
              });
              setChartData(
                {
                  labels: labels,
                  datasets: [
                    {
                      label: `Carbon Intensity for ${date}`,
                      data: datas,
                      backgroundColor: [
                        "#ffbb11",
                        "#ecf0f1",
                      ]          
                    }
                  ]
                }
            ); 
          }   
        }
        CarbonIntensity();

      }, [chartData]);
    
    

  
  return (
    <Layout
    >
      <div className="container">
        <div>      
          <input placeholder="from" name='from' type="datetime-local" />
          <input placeholder="to" name='to' type="datetime-local"/>
          <button>submit</button>
        </div>
        {chartData && <Chart1 chartData={chartData} />}
    </div>
    

    </Layout>
  )  
}

export default Intensity;
