import { useEffect, useState, useRef } from 'react';
import Link from 'next/link';
import Head from 'next/head';
import Image from 'next/image';
import { faSearch, } from "@fortawesome/free-solid-svg-icons";

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import styles from '../styles/layout.module.css';



const Layout = ({children}) => {


    useEffect(() => {
       
    }, []);
    
    return (
        <div className="" style={{position: 'relative'}}>
            <Head>
                <title>Viridios </title>
                <meta name="description" content="Light Echo" />
                <link rel="icon" href="/favicon.ico" />
            </Head>

            
            <header className={ styles.headerbox + ` flex justify-start md:justify-start border-t-4 border-blue-900 sticky h-16 bg-white z-10`}>
                    <h1 className="justify-around text-xl font-medium p-4">
                        <span className="text-blue-900">Viridios</span>
                    </h1>

            <nav>
                <ul>
                    <li><Link href="/"><a>Sustainable Development Goals</a></Link></li>
                    <li><Link href="/intensity"><a>Carbon Intensity</a></Link></li>
                    <li><Link href="/latex"><a>Markdown with LaTeX</a></Link></li>
                </ul>
            </nav>
            </header>
            <div className="z-10" style={{backgroundColor: '#f4f4f4'}}>
                {children}
            </div>
        </div>
    )

}

export default Layout;